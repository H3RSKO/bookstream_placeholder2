import React, { Component } from 'react';
import './App.css';
import { Formik, Field, Form } from 'formik';
import book from './reader.png'


class App extends Component {
  render() {
    return (
<div className="App" >
  <div className="container-fluid">
      <div className="row ">
        <div className="col-lg-8 exp">
          <div className="title">
            BOOKSTREAM
            <p className="subtitle">The $8 a month book club!</p>
          </div>
          <img className="book" src={book}/>
          <div className="subline">
            (Coming soon)
          </div>
        </div>
        <div className="col-lg-4 register">
          <div className="formContainer">
            <h1 className="registerNow">Register for Beta</h1>
            <Formik
             initialValues={{
               "bot-field": "",
               "form-name": "contact",
               email: "",
               firstname: "",
               lastname: ""
             }}>
              <form name="contact" method="POST" data-netlify="true" onSubmit="/success">
                <p>
                  <label>First Name: <p><input className="input" type="text" name="firstname" /></p></label>
                  <p><label>Last Name: <p><input className="input" type="text" name="lastname" /></p></label></p>
                  </p>
                    <label>Email: <p><input className="input" type="email" name="email" /></p></label>
                <p>
                  <button className="button" type="submit">Submit</button>
                  </p><Field type="hidden" name="bot-field" />
                  <Field type="hidden" name="form-name" />
                </form>
              </Formik>
            </div>
          </div>
      </div>
  </div>
</div>
    );
  }
}

export default App;
